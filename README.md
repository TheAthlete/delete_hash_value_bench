Запускать:

    zcat ids.gz | ./delete_hash_value.pl

Вывод бенчмарка:

    initialization hash...
    done.
    Keys in hash: 1000000
    delete hash value duplicates
    --- grep_map ---
    the total time is: 2.054143
    --- grep ---
    the total time is: 1.692204
    --- reverse ---
    the total time is: 1.942813
    --- while ---
    the total time is: 1.20926
