#!/usr/bin/env perl

# Запускать:
#
#   zcat ids.gz | ./delete_false_value_bench.pl
#
# TODO: Добавить help, если ничего не задано в STDIN

use strict;
use warnings;
use feature 'say';

use Time::HiRes qw/gettimeofday tv_interval/;
use Text::TabularDisplay;

use DDP;

use constant {
  TIME_START => 0,
  TIME_END => 1,
};

# 1. Инициализация хэша
#######################
# 1) получаем данные с файла (по STDIN)
my ($count, $h) = (0, {});
say 'initialization hash...';
while (<STDIN>) {
  chomp;
  $h->{++$count} = $_;
}
say 'done.';
say 'Keys in hash: '.scalar keys %$h;

# 2) присваиваем false в качестве значения для ключей, у которых значения повторяются (т.е. имеются дубли)
my %hr; 
# my ($cnt_false, $cnt_true) = (0, 0);
while (my ($k, $v) = each %$h) {
  $h->{$k} = "" if ++$hr{$v} > 1
  # if(++$hr{$v} > 1) {
  #   $h->{$k} = ""; 
  #   ++$cnt_false;
  # } else {
  #   ++$cnt_true;
  # }
}

# say "cnt false: $cnt_false, cnt true: $cnt_true";

# 3) увеличиваем хэш (дублируем значения исходного хэша)
$h = fill_hash($h) for 1..2;
say 'Keys in hash: '.scalar keys %$h;
#######################

my $t = Text::TabularDisplay->new(qw(name total_time cnt_keys_after_del));
my @table;

my %hn = %$h;

say 'delete hash with false values';

my $start = [gettimeofday];
delete @$h{grep {!$h->{$_}} keys %$h};
my $elapsed = tv_interval($start, [gettimeofday]);
push @table, ['grep', $elapsed, scalar keys %$h];

%$h = %hn;
%hr = ();
# say scalar keys %$h;

$start = [gettimeofday];
while (my ($k, $v) = each %$h) {
  delete $h->{$k} unless $v;
}
$elapsed = tv_interval($start, [gettimeofday]);
push @table, ['while', $elapsed, scalar keys %$h];

%$h = %hn;
%hr = ();
# say scalar keys %$h;

$t->add(@$_) for @table;
say $t->render;

sub fill_hash {
  my $hash = shift;

  my $max_key = (sort {$a <=> $b} keys %$hash)[-1] + 1;
  say $max_key;
  my $cnt = 0;
  for ($max_key .. 2*$max_key) {
    $hash->{$_} = $hash->{++$cnt} || "";
  }

  return $hash;
}

# +-------+------------+--------------------+
# | name  | total_time | cnt_keys_after_del |
# +-------+------------+--------------------+
# | grep  | 6.334587   | 380188             |
# | while | 4.107048   | 380188             |
# +-------+------------+--------------------+
