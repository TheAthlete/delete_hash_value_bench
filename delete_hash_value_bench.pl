#!/usr/bin/env perl

# Запускать:
#   zcat ids.gz | ./delete_hash_value.pl
#
# TODO: Добавить help, если ничего не задано в STDIN

use strict;
use warnings;
use feature 'say';

use Time::HiRes qw/gettimeofday tv_interval/;
use Benchmark qw(:all);

use DDP;

use constant {
  TIME_START => 0,
  TIME_END => 1,
};

# initialization
my $count = 0;
my %h;
say 'initialization hash...';
while (<STDIN>) {
  chomp;
  $h{$count++} = $_;
}
say 'done.';
say 'Keys in hash: '.scalar keys %h;

# TODO: реализовать запись значений времени в массив @total_time = ([$start, $end], ...)
my %hn = %h;
my %hr; 
my %h_new;

say 'delete hash value duplicates';

say '--- grep_map ---';
my $start = [gettimeofday];
%h = map {($_, $h{$_})} grep {++$hr{$h{$_}} == 1} keys %h;
my $elapsed = tv_interval($start, [gettimeofday]);
say "the total time is: $elapsed";
# say scalar keys %h;

%h = %hn;
%hr = ();
# say scalar keys %h;

say '--- grep ---';
$start = [gettimeofday];
delete @h{ grep {++$hr{$h{$_}} > 1} keys %h };
$elapsed = tv_interval($start, [gettimeofday]);
say "the total time is: $elapsed";
# say scalar keys %h;

%h = %hn;
%hr = ();
# say scalar keys %h;

say '--- reverse ---';
$start = [gettimeofday];
%h = reverse(%hr = reverse %h);
$elapsed = tv_interval($start, [gettimeofday]);
say "the total time is: $elapsed";
# say scalar keys %h;

%h = %hn;
%hr = ();
# say scalar keys %h;

say '--- while ---';
$start = [gettimeofday];
# вариант самый быстрый из всех, т.к. не создается копия списка, как в случае grep/map
while (my ($k, $v) = each %h) {
  delete $h{$k} if ++$hr{$v} > 1
}
# %h = reverse(%hr = reverse %h);
$elapsed = tv_interval($start, [gettimeofday]);
say "the total time is: $elapsed";
# say scalar keys %h;

%h = %hn;
%hr = ();
# say scalar keys %h;
